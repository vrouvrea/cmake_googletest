# CMake - GoogleTest - CppCheck

## Code source

This code is a copy of [this github repository](https://github.com/hANSIc99/cpp_testing_sample).

Copyright is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) - [Stephan Avenwedde](https://opensource.com/users/hansic99)

## The gitlab pipeline

```mermaid
graph LR;
  subgraph build
  B(Build);
  end
  subgraph test
  C(RunCTest);
  D(RunCppCheck);
  end
  B-->C
  B-->D
```

## Some references

If you want some explanation about the C++ code itself, the CMake part and how to build/debug the project with VSCodium,
please refer to [this related article](https://opensource.com/article/22/1/devops-cmake).

If case you need to know more about [GoogleTest](https://google.github.io/googletest/) (a C++ framework for unitary tests and mocking), [Ctest](https://cmake.org/cmake/help/latest/manual/ctest.1.html) (a CMake utility to launch unitary tests) and how to launch these tools, all is explained in [this second article](https://opensource.com/article/22/1/unit-testing-googletest-ctest).

[This final article](https://opensource.com/article/22/2/setup-ci-pipeline-gitlab) explains how to configure pipelines on https://gitlab.com. This part was adapted because on https://gitlab.inria.fr, you do not have the warranty to keep the same executor, and that the parallel jobs will be executed in the order defined in the `.gitlab-ci.yml`

## Modifications since the article version

* In `.gitlab-ci.yml`, `RunGTest` and `RunCTest` has been merged (as `RunGTest` was included in `RunCTest`), and another test job `RunCppCheck` launches [CppCheck](http://cppcheck.net/) - a static analysis tool for C and C++.

* The provided `Dockerfile` was used to generate the docker image `registry.gitlab.inria.fr/vrouvrea/cmake_googletest`.
```bash
docker build -t registry.gitlab.inria.fr/vrouvrea/cmake_googletest .
docker login registry.gitlab.inria.fr
docker push registry.gitlab.inria.fr/vrouvrea/cmake_googletest
```
  This image is used in `.gitlab-ci.yml` to fasten the build and because shared runners on https://gitlab.inria.fr are docker gitlab runners.

* In `CMakeLists.txt`, use a more modern `FetchContent_Declare` for GoogleTest
