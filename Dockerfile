# Image de base
FROM ubuntu:22.04

# Permet d'eviter l'installation du paquet tzdata, et de demander la 'time zone'
ENV DEBIAN_FRONTEND=noninteractive

# Installation des packages C++ avec apt-get
RUN apt-get update && apt-get install --no-install-recommends -y \
    ca-certificates \
    curl \
    make \
    cmake \
    g++ \
    cppcheck \
    git \
  && rm -rf /var/lib/apt/lists/*
